# vue-tabs
Straight forward Vue tab component.

## Install

```bash
yarn add @milkmedia/vue-tabs
# or 
npm i @milkmedia/vue-tabs
```

## Usage

```js
// …
import Tabs from "@milkmedia/vue-tabs";
// …
Vue.use(Tabs);
```

```html
    <tabs>
      <tab label="One" selected>Tab 1</tab>
      <tab label="Two">Tab 2</tab>
      <tab label="Three">Tab 3</tab>
    </tabs>
```

### More usage

https://codesandbox.io/s/pw6kwnmr4x